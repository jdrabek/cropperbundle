<?php

namespace CropperBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CropperBundle\Entity\AbstractImage;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * ModifiedImage
 *
 * @ORM\Table(name="modified_image")
 * @ORM\Entity(repositoryClass="CropperBundle\Repository\ModifiedImageRepository")
 */
class ModifiedImage extends AbstractImage {

    /**
     * @ManyToOne(targetEntity="OriginalImage", inversedBy="modifiedImages")     
     */
    private $original;

    /**
     * @var int
     *
     * @ORM\Column(name="crop_x", type="integer")
     */
    private $cropX;

    /**
     * @var int
     *
     * @ORM\Column(name="crop_y", type="integer")
     */
    private $cropY;

    /**
     * @var int
     *
     * @ORM\Column(name="crop_w", type="integer")
     */
    private $cropW;

    /**
     * @var int
     *
     * @ORM\Column(name="crop_h", type="integer")
     */
    private $cropH;

    function getOriginal() {
        return $this->original;
    }

    function setOriginal($original) {
        $this->original = $original;
    }

    function getCropX() {
        return $this->cropX;
    }

    function getCropY() {
        return $this->cropY;
    }

    function getCropW() {
        return $this->cropW;
    }

    function getCropH() {
        return $this->cropH;
    }

    function setCropX($cropX) {
        $this->cropX = $cropX;
    }

    function setCropY($cropY) {
        $this->cropY = $cropY;
    }

    function setCropW($cropW) {
        $this->cropW = $cropW;
    }

    function setCropH($cropH) {
        $this->cropH = $cropH;
    }

}
