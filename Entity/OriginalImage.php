<?php

namespace CropperBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CropperBundle\Entity\AbstractImage;
use Doctrine\ORM\Mapping\OneToMany;

/**
 * OriginalImage
 *
 * @ORM\Table(name="original_image")
 * @ORM\Entity(repositoryClass="CropperBundle\Repository\OriginalImageRepository")
 */
class OriginalImage extends AbstractImage {

    /**
     * @OneToMany(targetEntity="ModifiedImage", mappedBy="original")
     */
    private $modifiedImages;

    function getModifiedImages() {
        return $this->modifiedImages;
    }

    function setModifiedImages($modifiedImages) {
        $this->modifiedImages = $modifiedImages;
    }

}
