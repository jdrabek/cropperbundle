<?php

namespace CropperBundle\Controller;

use CropperBundle\Entity\OriginalImage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use CropperBundle\Entity\ModifiedImage;
use CropperBundle\Service\CropService;
use CropperBundle\Entity\AbstractImage;

/**
 * Image controller.
 *
 * @Route("cropper")
 */
class ImageController extends \AppBundle\Lib\AppController {

    /**
     * Lists all image entities.
     *
     * @Route("/", name="image_index")
     * @Method("GET")
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $images = $em->getRepository('CropperBundle:OriginalImage')->findAll();

        return $this->render('CropperBundle:image:index.html.twig', array(
                    'images' => $images,
        ));
    }

    /**
     * Creates a new image entity.
     *
     * @Route("/new", name="image_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        $image = new OriginalImage();
        $form = $this->createForm('CropperBundle\Form\OriginalImageType', $image);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $image->getFilename();

            $fileName = md5(uniqid()) . '.' . $file->guessExtension();

            // Move the file to the directory where brochures are stored
            $file->move(
                    $this->getParameter('images_directory'), $fileName
            );

            $image->setFilename($fileName);



            $em = $this->getDoctrine()->getManager();
            $em->persist($image);
            $em->flush($image);

            return $this->redirectToRoute('image_show', array('id' => $image->getId()));
        }

        return $this->render('CropperBundle:image:new.html.twig', array(
                    'image' => $image,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a image entity.
     *
     * @Route("/{id}", name="image_show")
     * @Method("GET")
     */
    public function showAction(AbstractImage $image) {
        if ($image instanceof OriginalImage) {
            $image = $this->getDoctrine()->getRepository('CropperBundle:OriginalImage')->find($image->getId());
            $view = 'CropperBundle:image:show_original.html.twig';
        } else {
            $view = 'CropperBundle:image:show_modified.html.twig';
            $image = $this->getDoctrine()->getRepository('CropperBundle:ModifiedImage')->find($image->getId());
        }
        $deleteForm = $this->createDeleteForm($image);

        return $this->render($view, array(
                    'image' => $image,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing image entity.
     *
     * @Route("/{id}/edit", name="image_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, AbstractImage $originalImage) {
        $deleteForm = $this->createDeleteForm($originalImage);
        $modifiedImage = new ModifiedImage();

        $editForm = $this->createForm('CropperBundle\Form\ModifiedImageType', $modifiedImage);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $cropper = $this->get('crop');
            $cropper->setCoordinates($modifiedImage->getCropX(), $modifiedImage->getCropY(), $modifiedImage->getCropW(), $modifiedImage->getCropH())
                    ->setInput($originalImage)
                    ->crop()
                    ->save();

            return $this->redirectToRoute('image_index', array('id' => $cropper->getModifiedImage()->getId()));
        }

        return $this->render('CropperBundle:image:edit.html.twig', array(
                    'image' => $originalImage,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a image entity.
     *
     * @Route("/{id}", name="image_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, AbstractImage $image) {
        $form = $this->createDeleteForm($image);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if($image instanceof OriginalImage){
                foreach($image->getModifiedImages() as $modifiedImage){
                    $em->remove($modifiedImage);
                }
            }
            $em->remove($image);
            $em->flush($image);
        }

        return $this->redirectToRoute('image_index');
    }

    /**
     * Creates a form to delete a image entity.
     *
     * @param OriginalImage $image The image entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($image) {        
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('image_delete', array('id' => $image->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
