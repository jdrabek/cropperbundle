<?php

namespace CropperBundle\Service;

use Doctrine\ORM\EntityManager;
use CropperBundle\Entity\OriginalImage;
use CropperBundle\Entity\ModifiedImage;

/**
 * Crop rectangle from given image 
 */
class CropService {

    /**
     * @var EntityManager
     */
    private $em;

    /**
     *
     * @var string Location to read original images and put modified images
     */
    private $imagesDirectory;

    /**
     * @var OriginalImage Original image entity
     */
    private $inputImage;

    /**
     * @var resource Output image resource
     */
    private $outputImageResource;

    /**
     *
     * @var string Output filename
     */
    private $outputFilename;

    /**
     * @var ModifiedImage Modified image entity
     */
    private $modifiedImage;

    /**
     * @var int   crop rectangle starting point x coordinate
     * @var int   crop rectangle starting point y coordinate
     * @var int   crop rectangle width
     * @var int   crop rectangle height
     */
    private $x, $y, $w, $h = NULL;

    public function __construct(EntityManager $entityManager, $imagesDirectory) {
        $this->em = $entityManager;
        $this->imagesDirectory = $imagesDirectory;
    }

    /**
     * Sets input image
     * @param OriginalImage $image
     * @return $this
     */
    public function setInput(OriginalImage $image) {
        $this->inputImage = $image;
        return $this;
    }

    /**
     * Sets crop frame coordinates
     * @param   int     $x   crop rectangle starting point x coordinate
     * @param   int     $y   crop rectangle starting point y coordinate
     * @param   int     $w   crop rectangle width
     * @param   int     $h   crop rectangle height
     * @return Crop
     */
    public function setCoordinates($x, $y, $w, $h) {
        list($this->x, $this->y, $this->w, $this->h) = func_get_args();
        return $this;
    }

    /**
     * Crops image
     * @return $this
     * @throws \Exception
     */
    public function crop() {
        if (is_null($this->x) || is_null($this->y) || is_null($this->w) || is_null($this->h)) {
            throw new \Exception('Any of coordinates cannot be null');
        }
        $iSrc = imagecreatefromjpeg($this->imagesDirectory . '/' . $this->inputImage->getFilename());
        $this->outputImageResource = imagecrop($iSrc, ['x' => $this->x, 'y' => $this->y, 'width' => $this->w, 'height' => $this->h]);
        if ($this->outputImageResource !== FALSE) {
            return $this;
        }
        throw new \Exception('GD crop function failed');
    }

    /**
     * Save output image resource to file
     * @return boolean
     */
    private function saveToFile() {
        $this->outputFilename = uniqid('modifiedImage_') . '.png';
        $destPath = $this->imagesDirectory . '/' . $this->outputFilename;
        return imagepng($this->outputImageResource, $destPath);
    }

    /**
     * Saves output image to database
     * @return boolean
     */
    private function saveToDb() {
        $modifiedImage = new ModifiedImage();
        $modifiedImage->setOriginal($this->inputImage);
        $modifiedImage->setFilename($this->outputFilename);
        $modifiedImage->setCropX($this->x);
        $modifiedImage->setCropY($this->y);
        $modifiedImage->setCropW($this->w);
        $modifiedImage->setCropH($this->h);
        $this->em->persist($modifiedImage);
        $this->em->flush();
        $this->modifiedImage = $modifiedImage;
        return null !== $modifiedImage->getId();
    }

    /**
     * Saves output image resource to file and database
     * @return boolean
     * @throws \Exception
     */
    public function save() {
        if (!$this->saveToFile()) {
            throw new \Exception('Cannot save cropped image to file');
        }
        if (!$this->saveToDb()) {
            throw new \Exception('Cannot save cropped image to database');
        }
        return true;
    }

    /**
     * Get modified image entity
     * @return ModifiedImage
     */
    public function getModifiedImage() {
        return $this->modifiedImage;
    }

}
